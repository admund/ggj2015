package me.admund.ggj2015.physics;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import me.admund.framework.draw.DrawUtils;
import me.admund.framework.draw.SimpleTextureHolder;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.framework.physics.objects.PhysicsRect;
import me.admund.ggj2015.TextureList;
import me.admund.ggj2015.levels.PhysicsObjectDesc;

/**
 * Created by admund on 2015-01-24.
 */
public class Bucket extends PhysicsRect {
    public static final String name = "Bucket";

    public static Bucket createObj(PhysicsObjectDesc desc, PhysicsWorld world) {
        Bucket bucket = new Bucket();
        bucket.create(world);
        bucket.init(desc.getFloat(PhysicsObjectDesc.POS_X), desc.getFloat(PhysicsObjectDesc.POS_Y),
                desc.getFloat(PhysicsObjectDesc.SIZE_X), desc.getFloat(PhysicsObjectDesc.SIZE_Y));
        return bucket;
    }

    public void init(float posX, float posY, float sizeX, float sizeY) {
        super.init();
        setCurrentPos(posX, posY);
        setSize(sizeX, sizeY);
        setOrigin(Align.center);
        setTextureHolder(new SimpleTextureHolder(TextureList.bucket));
    }

    @Override
    public BodyDef getBodyDef() {
        BodyDef bodyDef = super.getBodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        return bodyDef;
    }

    @Override
    public FixtureDef getFixtureDef() {
        FixtureDef def = super.getFixtureDef();
        def.density = .1f;
        return def;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        DrawUtils.drawActor(batch, getTexture(), this);
    }

    @Override
    public boolean isGround() {
        return true;
    }

    @Override
    public void beginContact(Contact contact, boolean isObjectA) {}

    @Override
    public void endContact(Contact contact, boolean isObjectA) {}

    @Override
    public void preSolve(Contact contact, boolean isObjectA) {}

    @Override
    public void postSolve(Contact contact, boolean isObjectA) {}
}
