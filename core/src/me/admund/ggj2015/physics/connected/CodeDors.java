package me.admund.ggj2015.physics.connected;

import com.badlogic.gdx.graphics.g2d.Batch;
import me.admund.framework.draw.DrawObject;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.ggj2015.levels.PhysicsObjectDesc;
import me.admund.ggj2015.physics.CodeDorsElement;

/**
 * Created by admund on 2015-01-24.
 */
public class CodeDors extends DrawObject {
    public static final String name = "CodeDors";

    public static CodeDors createObj(PhysicsObjectDesc desc, PhysicsWorld world) {
        CodeDors dors = new CodeDors();
        dors.create(world);
        dors.init(desc.getFloat(PhysicsObjectDesc.POS_X), desc.getFloat(PhysicsObjectDesc.POS_Y),
                desc.getFloat(PhysicsObjectDesc.SIZE_X), desc.getFloat(PhysicsObjectDesc.SIZE_Y));
        return dors;
    }

    private CodeDorsElement upWall = null;
    private CodeDorsElement downWall = null;
    private CodeDorsElement upDors = null;
    private CodeDorsElement downDors = null;

    public void init(float posX, float posY, float sizeX, float sizeY) {
        downWall.init(posX, posY + (sizeY * .05f), sizeX, sizeY * .1f);
        downDors.init(posX, posY + (sizeY * (.1f + .15f)), sizeX, sizeY * .3f);
        upDors.init(posX, posY + (sizeY * (.4f + .15f)), sizeX, sizeY * .3f);
        upWall.init(posX, posY + (sizeY * (.7f + .15f)), sizeX, sizeY * .3f);
    }

    public void create(PhysicsWorld world) {
        upWall = new CodeDorsElement(true);
        upWall.create(world);
        upWall.setDors(this);

        downWall = new CodeDorsElement(false);
        downWall.create(world);
        downWall.setDors(this);

        upDors = new CodeDorsElement(true);
        upDors.create(world);
        upDors.setDors(this);

        downDors = new CodeDorsElement(false);
        downDors.create(world);
        downDors.setDors(this);
    }

    private float MAX_OPEN_TIME = 3f;
    private float openTime = -1;

    public void act(float delta) {
        if(openTime != -1 && openTime != 0f) {
            openTime -= delta;
            if(openTime < 0f) {
                openTime = 0;
            }

            downDors.updatePos(1 - (openTime/MAX_OPEN_TIME));
            upDors.updatePos(1 - (openTime/MAX_OPEN_TIME));
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        upWall.draw(batch, parentAlpha);
        downWall.draw(batch, parentAlpha);
        upDors.draw(batch, parentAlpha);
        downDors.draw(batch, parentAlpha);
    }

    public void open() {
        openTime = MAX_OPEN_TIME;
    }
}
