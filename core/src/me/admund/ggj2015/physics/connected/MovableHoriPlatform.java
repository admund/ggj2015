package me.admund.ggj2015.physics.connected;

import me.admund.framework.physics.PhysicsWorld;
import me.admund.ggj2015.levels.PhysicsObjectDesc;

/**
 * Created by admund on 2015-01-24.
 */
public class MovableHoriPlatform extends AbstractMovable {
    public static final String name = "MovableHoriPlatform";

    public static MovableHoriPlatform createObj(PhysicsObjectDesc desc, PhysicsWorld world) {
        MovableHoriPlatform platform = new MovableHoriPlatform();
        platform.create(world);
        platform.init(desc.getFloat(PhysicsObjectDesc.POS_X), desc.getFloat(PhysicsObjectDesc.POS_Y),
                desc.getFloat(PhysicsObjectDesc.SIZE_X), desc.getFloat(PhysicsObjectDesc.SIZE_Y),
                desc.getFloat(PhysicsObjectDesc.RANGE));
        platform.setNeededActorId(desc.getInt(PhysicsObjectDesc.NEEDED_ACTOR_ID));
        return platform;
    }

    private float range = 0;
    private float startPosX = 0;
    private float currentValue = 0f;
    private boolean isGrowing = true;

    public void init(float x, float y, float width, float height, float range) {
        super.init(x, y, width, height);
        this.range = range;
        this.startPosX = x;
    }

    @Override
    public void move(float delta, float progress) {
        if(progress != 0f) {
            if(isGrowing) {
                currentValue += .25f * delta;
                if(currentValue > 1f) {
                    isGrowing = false;
                }
            } else {
                currentValue -= .25f * delta;
                if(currentValue < 0) {
                    isGrowing = true;
                }
            }
            setCurrentPos(startPosX + range * currentValue, getPosition().y);
        }
    }
}
