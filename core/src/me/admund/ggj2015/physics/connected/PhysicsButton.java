package me.admund.ggj2015.physics.connected;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import me.admund.framework.draw.DrawObject;
import me.admund.framework.draw.DrawUtils;
import me.admund.framework.draw.SimpleTextureHolder;
import me.admund.framework.physics.PhysicsObject;
import me.admund.framework.physics.PhysicsUtils;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.ggj2015.GGJ2015Game;
import me.admund.ggj2015.TextureList;
import me.admund.ggj2015.draw.SimpleDrawObject;
import me.admund.ggj2015.levels.PhysicsObjectDesc;
import me.admund.ggj2015.physics.CollisionFilters;

/**
 * Created by admund on 2015-01-24.
 */
public class PhysicsButton extends PhysicsObject {
    public static final String name = "PhysicsButton";

    public static PhysicsButton createObj(PhysicsObjectDesc desc, PhysicsWorld world) {
        PhysicsButton button = new PhysicsButton();
        button.create(world);
        button.init(desc.getFloat(PhysicsObjectDesc.POS_X), desc.getFloat(PhysicsObjectDesc.POS_Y),
                desc.getFloat(PhysicsObjectDesc.SIZE_X), desc.getFloat(PhysicsObjectDesc.SIZE_Y));
        button.setId(desc.getInt(PhysicsObjectDesc.ID));
        return button;
    }

    private boolean isPushed = false;
    private boolean isGoingDown = false;

    private float MAX_COMPRESS = .5f;
    private float currentCompress = 1f;

    private float startPosX = 0;
    private float startPosY = 0;

    private float sizeX = 0;
    private float sizeY = 0;

    private SimpleDrawObject botoom = null;

    public PhysicsButton() {
        super();
    }

    @Override
    public boolean isGround() {
        return true;
    }

    public void init(float posX, float posY, float sizeX, float sizeY) {
        super.init();
        setSize(sizeX, sizeY);
        setCurrentPos(posX, posY);

        this.startPosX = posX;
        this.startPosY = posY - sizeY * .5f;

        this.sizeX = sizeX;
        this.sizeY = sizeY;

        setOrigin(Align.center);

        setTextureHolder(new SimpleTextureHolder("button2.png"));

        botoom = new SimpleDrawObject(new SimpleTextureHolder("button1.png"));
        botoom.init(posX, posY - (sizeY * .5f), sizeX, sizeY * .5f);
        botoom.setOrigin(Align.center);
    }

    public float getProgress() {
        return 1 -(currentCompress - (1 - MAX_COMPRESS))/MAX_COMPRESS;
    }

    public void setPushed(boolean isPushed) {
        this.isPushed = isPushed;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        changeCompress(delta);
    }

    private void changeCompress(float delta) {
        boolean sizeChanged = false;
        if(isPushed || isGoingDown) {
            isGoingDown = true;
            currentCompress -= MAX_COMPRESS * delta;

            sizeChanged = true;

            if(currentCompress < MAX_COMPRESS) {
                currentCompress = MAX_COMPRESS;
                isGoingDown = false;
            }

        } else if(currentCompress != 1f) {
            currentCompress += MAX_COMPRESS * delta;

            sizeChanged = true;

            if(currentCompress > 1f) {
                currentCompress = 1f;
            }
        }

        if(sizeChanged) {
            setSize(sizeX, sizeY * currentCompress);
            setCurrentPos(startPosX, startPosY + (sizeY * currentCompress) * .5f);
            getBody().setAwake(true);
        }
    }

    @Override
    public BodyDef getBodyDef() {
        BodyDef def = new BodyDef();
        def.active = true;
        def.fixedRotation = true;
        def.type = BodyDef.BodyType.StaticBody;
        return def;
    }

    @Override
    public FixtureDef getFixtureDef() {
        FixtureDef def = new FixtureDef();
        def.shape = PhysicsUtils.getDefaultPolygonShape();
        def.density = 3f/(sizeX * sizeY);
        return def;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        DrawUtils.drawActor(batch, getTexture(), this);
        botoom.draw(batch, parentAlpha);
    }

    @Override
    public void beginContact(Contact contact, boolean isObjectA) {

    }

    @Override
    public void endContact(Contact contact, boolean isObjectA) {

    }

    @Override
    public void preSolve(Contact contact, boolean isObjectA) {

    }

    @Override
    public void postSolve(Contact contact, boolean isObjectA) {

    }
}
