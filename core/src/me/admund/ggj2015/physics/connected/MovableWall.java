package me.admund.ggj2015.physics.connected;

import me.admund.framework.physics.PhysicsWorld;
import me.admund.ggj2015.levels.PhysicsObjectDesc;

/**
 * Created by admund on 2015-01-24.
 */
public class MovableWall extends AbstractMovable {
    public static final String name = "MovableWall";

    public static MovableWall createObj(PhysicsObjectDesc desc, PhysicsWorld world) {
        MovableWall wall = new MovableWall();
        wall.create(world);
        wall.init(desc.getFloat(PhysicsObjectDesc.POS_X), desc.getFloat(PhysicsObjectDesc.POS_Y),
                desc.getFloat(PhysicsObjectDesc.SIZE_X), desc.getFloat(PhysicsObjectDesc.SIZE_Y),
                desc.getFloat(PhysicsObjectDesc.RANGE));
        wall.setNeededActorId(desc.getInt(PhysicsObjectDesc.NEEDED_ACTOR_ID));
        return wall;
    }

    private float startPosY = 0;
    private float moveRangeY = 0;

    public void init(float x, float y, float width, float height, float moveRangeX) {
        super.init(x, y, width, height);
        this.moveRangeY = moveRangeX;
        this.startPosY = y;
    }

    @Override
    public void move(float delta, float progress) {
        setCurrentPos(getPosition().x, startPosY + moveRangeY * progress);
    }
}
