package me.admund.ggj2015.physics.connected;

import me.admund.framework.physics.PhysicsWorld;
import me.admund.ggj2015.levels.PhysicsObjectDesc;

/**
 * Created by admund on 2015-01-24.
 */
public class MovableVerPlatform extends AbstractMovable {
    public static final String name = "MovableVerPlatform";

    public static MovableVerPlatform createObj(PhysicsObjectDesc desc, PhysicsWorld world) {
        MovableVerPlatform platform = new MovableVerPlatform();
        platform.create(world);
        platform.init(desc.getFloat(PhysicsObjectDesc.POS_X), desc.getFloat(PhysicsObjectDesc.POS_Y),
                desc.getFloat(PhysicsObjectDesc.SIZE_X), desc.getFloat(PhysicsObjectDesc.SIZE_Y),
                desc.getFloat(PhysicsObjectDesc.RANGE));
        platform.setNeededActorId(desc.getInt(PhysicsObjectDesc.NEEDED_ACTOR_ID));
        return platform;
    }

    private float range = 0;
    private float startPosY = 0;
    private float currentValue = 0f;
    private boolean isGrowing = true;
    private float deltaGrow = 0;
    private float MAX_SPEED = 5;

    public void init(float x, float y, float width, float height, float range) {
        super.init(x, y, width, height);
        this.range = range;
        this.startPosY = y;
        this.deltaGrow = 1f/ (range/MAX_SPEED);
    }

    @Override
    public void move(float delta, float progress) {
        if(progress != 0f) {
            if(isGrowing) {
                currentValue += deltaGrow * delta;
                if(currentValue > 1f) {
                    isGrowing = false;
                }
            } else {
                currentValue -= deltaGrow * delta;
                if(currentValue < 0) {
                    isGrowing = true;
                }
            }
            setCurrentPos(getPosition().x, startPosY + range * currentValue);
        }
    }
}
