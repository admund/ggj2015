package me.admund.ggj2015.physics.connected;

import com.badlogic.gdx.physics.box2d.FixtureDef;
import me.admund.framework.physics.ActorId;
import me.admund.ggj2015.physics.Wall;

/**
 * Created by admund on 2015-01-24.
 */
public abstract class AbstractMovable extends Wall {
    private PhysicsButton button = null;

    @Override
    public boolean isGround() {
        return true;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if(button.getProgress() != 0) {
            move(delta, button.getProgress());
        }
    }

    @Override
    public FixtureDef getFixtureDef() {
        FixtureDef def = super.getFixtureDef();
        def.friction = .4f;
        return def;
    }

    @Override
    public void setNeededActorId(ActorId actor) {
        if(actor instanceof PhysicsButton) {
            button = (PhysicsButton) actor;
        } else {
            throw new ClassCastException("Wrong ActorId im " + this.getClass().toString());
        }
    }

    public abstract void move(float delta, float progress);
}
