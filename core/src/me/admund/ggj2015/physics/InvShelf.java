package me.admund.ggj2015.physics;

import com.badlogic.gdx.graphics.g2d.Batch;
import me.admund.framework.draw.SimpleTextureHolder;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.ggj2015.TextureList;
import me.admund.ggj2015.levels.PhysicsObjectDesc;

/**
 * Created by admund on 2015-01-25.
 */
public class InvShelf extends Shelf {
    public static final String name = "InvShelf";

    public static InvShelf createObj(PhysicsObjectDesc desc, PhysicsWorld world) {
        InvShelf shelf = new InvShelf();
        shelf.create(world);
        shelf.init(desc.getFloat(PhysicsObjectDesc.POS_X), desc.getFloat(PhysicsObjectDesc.POS_Y),
                desc.getFloat(PhysicsObjectDesc.SIZE_X), desc.getFloat(PhysicsObjectDesc.SIZE_Y));
        //shelf.setTextureHolder(new SimpleTextureHolder(TextureList.shelf));
        return shelf;
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        //super.draw(batch, parentAlpha);
    }
}
