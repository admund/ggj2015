package me.admund.ggj2015.physics;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import me.admund.framework.draw.DrawObject;
import me.admund.framework.physics.PhysicsObject;
import me.admund.framework.physics.PhysicsUtils;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.ggj2015.levels.PhysicsObjectDesc;

/**
 * Created by admund on 2015-01-24.
 */
public class Teeter extends DrawObject {
    public static final String name = "Teeter";

    private PhysicsWorld world = null;

    public static Teeter createObj(PhysicsObjectDesc desc, PhysicsWorld world) {
        Teeter teeter = new Teeter();
        teeter.create(world);
        teeter.init(desc.getFloat(PhysicsObjectDesc.POS_X), desc.getFloat(PhysicsObjectDesc.POS_Y),
                desc.getFloat(PhysicsObjectDesc.SIZE_X), desc.getFloat(PhysicsObjectDesc.SIZE_Y));
        return teeter;
    }


    private TeeterLeg leg = null;
    private TeeterLever lever = null;

    public void create(PhysicsWorld world) {
        leg = new TeeterLeg();
        leg.create(world);
        leg.init();

        lever = new TeeterLever();
        lever.create(world);
        lever.init();

        this.world = world;
    }

    public void init(float posX, float posY, float sizeX, float sizeY) {
        leg.init(posX, posY, sizeX, sizeY);
        lever.init(posX, posY, sizeX, sizeY);

        RevoluteJointDef jointDef = new RevoluteJointDef();
        //jointDef.referenceAngle = 0;
        jointDef.localAnchorA.set(0, sizeY * 1.25f);
        jointDef.localAnchorB.set(0, 0);
        jointDef.enableMotor = false;
        jointDef.bodyA = leg.getBody();
        jointDef.bodyB = lever.getBody();
        //jointDef.collideConnected = false;

        world.createJoint(jointDef);
    }

    class TeeterLeg extends PhysicsObject {

        public void init(float posX, float posY, float sizeX, float sizeY) {
            super.init();
            setCurrentPos(posX, posY);
            setSize(sizeX * .2f, sizeY, getShapeVerticles(sizeX * .2f, sizeY));
        }

        private Vector2[] getShapeVerticles(float sizeX, float sizeY) {
            Vector2[] verticles = new Vector2[3];
            verticles[0] = new Vector2(sizeX * -.5f, 0);
            verticles[1] = new Vector2(sizeX * .5f, 0);
            verticles[2] = new Vector2(0, sizeY);
            return  verticles;
        }

        @Override
        public BodyDef getBodyDef() {
            BodyDef bodyDef = new BodyDef();
            bodyDef.type = BodyDef.BodyType.StaticBody;//DynamicBody;
            return bodyDef;
        }

        @Override
        public FixtureDef getFixtureDef() {
            FixtureDef def = new FixtureDef();
            def.shape = PhysicsUtils.getDefaultPolygonShape();
            return def;
        }

        @Override
        public boolean isGround() {
            return false;
        }

        @Override
        public void beginContact(Contact contact, boolean isObjectA) {}

        @Override
        public void endContact(Contact contact, boolean isObjectA) {}

        @Override
        public void preSolve(Contact contact, boolean isObjectA) {}

        @Override
        public void postSolve(Contact contact, boolean isObjectA) {}
    }

    class TeeterLever extends PhysicsObject {

        public void init(float posX, float posY, float sizeX, float sizeY) {
            super.init();
            setCurrentPos(posX + sizeX * .25f, posY + sizeY * 2);
            setSize(sizeX, sizeY * .2f, getShapeVerticles(sizeX, sizeY * .2f));
        }

        private Vector2[] getShapeVerticles(float sizeX, float sizeY) {
            Vector2[] verticles = new Vector2[4];
            verticles[0] = new Vector2(sizeX * -.75f, sizeY * -.5f);
            verticles[1] = new Vector2(sizeX * -.75f, sizeY * .5f);
            verticles[2] = new Vector2(sizeX * .25f, sizeY * .5f);
            verticles[3] = new Vector2(sizeX * .25f, sizeY * -.5f);
            return  verticles;
        }

        @Override
        public BodyDef getBodyDef() {
            BodyDef bodyDef = new BodyDef();
            bodyDef.type = BodyDef.BodyType.DynamicBody;
            bodyDef.bullet = true;
            return bodyDef;
        }

        @Override
        public FixtureDef getFixtureDef() {
            FixtureDef def = new FixtureDef();
            def.shape = PhysicsUtils.getDefaultPolygonShape();
            def.density = 1;
            return def;
        }

        @Override
        public boolean isGround() {
            return true;
        }

        @Override
        public void beginContact(Contact contact, boolean isObjectA) {}

        @Override
        public void endContact(Contact contact, boolean isObjectA) {}

        @Override
        public void preSolve(Contact contact, boolean isObjectA) {}

        @Override
        public void postSolve(Contact contact, boolean isObjectA) {}
    }
}
