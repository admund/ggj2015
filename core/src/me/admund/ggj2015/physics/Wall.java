package me.admund.ggj2015.physics;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import me.admund.framework.draw.DrawUtils;
import me.admund.framework.draw.SimpleTextureHolder;
import me.admund.framework.draw.TextureRepo;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.framework.physics.objects.PhysicsRect;
import me.admund.ggj2015.TextureList;
import me.admund.ggj2015.levels.PhysicsObjectDesc;

/**
 * Created by admund on 2015-01-24.
 */
public class Wall extends PhysicsRect {
    public static final String name = "Wall";

    public static Wall createObj(PhysicsObjectDesc desc, PhysicsWorld world) {
        Wall wall = new Wall();
        wall.create(world);
        wall.init(desc.getFloat(PhysicsObjectDesc.POS_X), desc.getFloat(PhysicsObjectDesc.POS_Y),
                desc.getFloat(PhysicsObjectDesc.SIZE_X), desc.getFloat(PhysicsObjectDesc.SIZE_Y));
        return wall;
    }

    public Wall() {
        setTextureHolder(new SimpleTextureHolder(TextureList.wall));
    }

    @Override
    public FixtureDef getFixtureDef() {
        FixtureDef def = super.getFixtureDef();
        def.friction = 0;
        return def;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        DrawUtils.drawActor(batch, getTexture(), this);
    }
}
