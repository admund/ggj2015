package me.admund.ggj2015.physics;

/**
 * Created by admund on 2015-01-24.
 */
public class CollisionFilters {
    // DEFAULT = 0x0001;                                    // 0000000000000001 in binary
    public static final short CATEGORY_PLAYER = 0x0002;    // 0000000000000010 in binary
    //public static final short CATEGORY_BABY = 0x0004;               // 0000000000000100 in binary
    //public static final short CATEGORY_ENEMIES = 0x0008;            // 0000000000001000 in binary
    //public static final short CATEGORY_COIN = 0x0010;               // 0000000000010000 in binary

    public static final short MASK_PLAYER = ~CATEGORY_PLAYER;
    //public static final short MASK_SHELF_1 = CATEGORY_PLAYER;
    //public static final short MASK_SHELF_2 = ~CATEGORY_PLAYER;

    //public static final short MASK_ENEMIES = CATEGORY_PLAYER;
    //public static final short MASK_COIN = CATEGORY_PLAYER;
}
