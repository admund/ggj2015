package me.admund.ggj2015.physics;

import me.admund.framework.physics.PhysicsWorld;
import me.admund.framework.physics.objects.PhysicsRect;
import me.admund.ggj2015.levels.PhysicsObjectDesc;

/**
 * Created by admund on 2015-01-23.
 */
public class Ground extends PhysicsRect {
    public static final String name = "Ground";

    public static Ground createObj(PhysicsObjectDesc desc, PhysicsWorld world) {
        Ground ground = new Ground();
        ground.create(world);
        ground.init(desc.getFloat(PhysicsObjectDesc.POS_X), desc.getFloat(PhysicsObjectDesc.POS_Y),
                desc.getFloat(PhysicsObjectDesc.SIZE_X), desc.getFloat(PhysicsObjectDesc.SIZE_Y));
        return ground;
    }

    @Override
    public boolean isGround() {
        return true;
    }
}
