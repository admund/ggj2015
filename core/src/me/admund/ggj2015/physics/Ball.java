package me.admund.ggj2015.physics;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.framework.physics.objects.PhysicsCircle;
import me.admund.ggj2015.levels.PhysicsObjectDesc;

/**
 * Created by admund on 2015-01-24.
 */
public class Ball extends PhysicsCircle {
    public static final String name = "Ball";

    public static Ball createObj(PhysicsObjectDesc desc, PhysicsWorld world) {
        Ball ball = new Ball();
        ball.create(world);
        ball.init(desc.getFloat(PhysicsObjectDesc.POS_X), desc.getFloat(PhysicsObjectDesc.POS_Y),
                desc.getFloat(PhysicsObjectDesc.RADIUS));
        return ball;
    }

    public void init(float posX, float posY, float radius) {
        super.init();
        setCurrentPos(posX, posY);
        setSize(radius);
        // TODO setTextureHolder(new SimpleTextureHolder(TextureList.bucket));
    }

    @Override
    public BodyDef getBodyDef() {
        BodyDef bodyDef = super.getBodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        return bodyDef;
    }

    @Override
    public FixtureDef getFixtureDef() {
        FixtureDef def = super.getFixtureDef();
        def.density = 5f;
        return def;
    }
}
