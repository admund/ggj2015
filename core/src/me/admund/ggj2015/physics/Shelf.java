package me.admund.ggj2015.physics;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import me.admund.framework.draw.DrawUtils;
import me.admund.framework.draw.SimpleTextureHolder;
import me.admund.framework.physics.PhysicsObject;
import me.admund.framework.physics.PhysicsUtils;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.ggj2015.TextureList;
import me.admund.ggj2015.levels.PhysicsObjectDesc;

/**
 * Created by admund on 2015-01-24.
 */
public class Shelf extends PhysicsObject {
    public static final String name = "Shelf";

    public static Shelf createObj(PhysicsObjectDesc desc, PhysicsWorld world) {
        Shelf shelf = new Shelf();
        shelf.create(world);
        shelf.init(desc.getFloat(PhysicsObjectDesc.POS_X), desc.getFloat(PhysicsObjectDesc.POS_Y),
                desc.getFloat(PhysicsObjectDesc.SIZE_X), desc.getFloat(PhysicsObjectDesc.SIZE_Y));
        shelf.setTextureHolder(new SimpleTextureHolder(TextureList.shelf));
        return shelf;
    }


    @Override
    public BodyDef getBodyDef() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.bullet = true;
        return bodyDef;
    }

    @Override
    public FixtureDef getFixtureDef() {
        FixtureDef def = new FixtureDef();
        def.shape = PhysicsUtils.getDefaultPolygonShape();
        return def;
    }

    public void init(float x, float y, float width, float height) {
        super.init();
        setSize(width, height);
        setOrigin(Align.center);
        setCurrentPos(x, y);
        PhysicsUtils.updateRectShape(getShape(), width * .5f, height * .5f);
    }

    @Override
    public boolean isGround() {
        return true;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        DrawUtils.drawActor(batch, getTexture(), this);
    }

    @Override
    public void beginContact(Contact contact, boolean isObjectA) {}

    @Override
    public void endContact(Contact contact, boolean isObjectA) {}

    @Override
    public void preSolve(Contact contact, boolean isObjectA) {}

    @Override
    public void postSolve(Contact contact, boolean isObjectA) {}
}
