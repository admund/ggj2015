package me.admund.ggj2015.physics;

import com.badlogic.gdx.graphics.g2d.Batch;
import me.admund.framework.draw.DrawUtils;
import me.admund.framework.draw.SimpleTextureHolder;
import me.admund.framework.physics.objects.PhysicsRect;
import me.admund.ggj2015.TextureList;
import me.admund.ggj2015.physics.connected.CodeDors;

/**
 * Created by admund on 2015-01-24.
 */
public class CodeDorsElement extends PhysicsRect {
    private CodeDors dors = null;

    private float startPosY = 0;
    private float sizeY = 0;

    private boolean isUp = false;

    public CodeDorsElement(boolean isUp) {
        this.isUp = isUp;
    }

    @Override
    public void init(float x, float y, float width, float height) {
        super.init(x, y, width, height);
        this.startPosY = y;
        this.sizeY = height;

        setTextureHolder(new SimpleTextureHolder(TextureList.shelf));
    }

    public void setDors(CodeDors dors) {
        this.dors = dors;
    }

    public CodeDors getDors() {
        return dors;
    }

    public void updatePos(float progress) {
        progress *= isUp ? 1 : -1;
        setCurrentPos(getPosition().x, startPosY + sizeY * progress);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        DrawUtils.drawActor(batch, getTexture(), this);
    }
}
