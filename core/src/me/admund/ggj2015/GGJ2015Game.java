package me.admund.ggj2015;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import me.admund.framework.game.AbstractGame;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.framework.scenes.ScenesManager;
import me.admund.ggj2015.scenes.MainScene;
import me.admund.ggj2015.scenes.StartScene;

/**
 * Created by admund on 2015-01-23.
 */
public class GGJ2015Game extends AbstractGame {
    public static float WORLD_SIZE_X = PhysicsWorld.BOX_SCREEN_WIDTH  * 2 * 5;
    public static float WORLD_SIZE_Y = PhysicsWorld.BOX_SCREEN_HEIGHT;

    @Override
    public void create() {
        super.create();
        ScenesManager.inst().push( new StartScene(), true);

        musicPlay();
    }

    private void musicPlay() {
        Music theme = Gdx.audio.newMusic(Gdx.files.internal("uls.ogg"));
        theme.setLooping(true);
        theme.play();
    }
}
