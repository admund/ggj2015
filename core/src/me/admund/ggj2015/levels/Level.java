package me.admund.ggj2015.levels;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Logger;
import me.admund.framework.physics.ActorId;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.ggj2015.draw.SimpleDrawObject;
import me.admund.ggj2015.physics.*;
import me.admund.ggj2015.physics.connected.*;

/**
 * Created by admund on 2015-01-24.
 */
public class Level {

    private String fileName = "level_1.txt";

    private PhysicsWorld world = null;
    private Stage stage = null;

    private Array<PhysicsObjectDesc> objectList = new Array<PhysicsObjectDesc>();
    private Array<ActorId> actorList = new Array<ActorId>();

    public void create(PhysicsWorld world, Stage stage) {
        this.world = world;
        this.stage = stage;

        readFile();
        createObjects();
    }

    private void readFile() {
        FileHandle file = Gdx.files.internal(fileName);

        String separator = Gdx.app.getType() == Application.ApplicationType.Desktop ?
                System.getProperty("line.separator") : "#";

        String[] lines = file.readString().split(separator);
        PhysicsObjectDesc desc = new PhysicsObjectDesc();
        for(int i=0; i<lines.length; i++) {
            String line = lines[i];
            if(line.contains("///")) {
                continue;
            } else if(line.contains("---")) {
                objectList.add(desc);
                desc = new PhysicsObjectDesc();
            } else {
                String[] keyValue = line.split(" ");
                desc.argMap.put(keyValue[0], keyValue[1]);
            }
        }
    }

    private void createObjects() {
        for(int i=0; i<objectList.size; i++) {
            PhysicsObjectDesc desc = objectList.get(i);
            ActorId actor = getObj(desc);
            stage.addActor(actor);
            actorList.add(actor);
        }

        updateId();
    }

    private ActorId getObj(PhysicsObjectDesc desc) {
        if (desc.getString(PhysicsObjectDesc.CLASS_NAME).equals(Wall.name)) {
            return Wall.createObj(desc, world);

        } else if (desc.getString(PhysicsObjectDesc.CLASS_NAME).equals(Ground.name)) {
            return Ground.createObj(desc, world);

        } else if (desc.getString(PhysicsObjectDesc.CLASS_NAME).equals(SimpleDrawObject.name)) {
            return SimpleDrawObject.createObj(desc, world);

        } else if(desc.getString(PhysicsObjectDesc.CLASS_NAME).equals(PhysicsButton.name)) {
            return PhysicsButton.createObj(desc, world);

        } else if(desc.getString(PhysicsObjectDesc.CLASS_NAME).equals(MovableWall.name)) {
            return MovableWall.createObj(desc, world);

        } else if(desc.getString(PhysicsObjectDesc.CLASS_NAME).equals(MovableHoriPlatform.name)) {
            return MovableHoriPlatform.createObj(desc, world);

        } else if(desc.getString(PhysicsObjectDesc.CLASS_NAME).equals(MovableVerPlatform.name)) {
            return MovableVerPlatform.createObj(desc, world);

        } else if(desc.getString(PhysicsObjectDesc.CLASS_NAME).equals(Shelf.name)) {
            return Shelf.createObj(desc, world);

        } else if(desc.getString(PhysicsObjectDesc.CLASS_NAME).equals(InvShelf.name)) {
            return InvShelf.createObj(desc, world);

        } else if(desc.getString(PhysicsObjectDesc.CLASS_NAME).equals(Teeter.name)) {
            return Teeter.createObj(desc, world);

        } else if(desc.getString(PhysicsObjectDesc.CLASS_NAME).equals(Bucket.name)) {
            return Bucket.createObj(desc, world);

        } else if(desc.getString(PhysicsObjectDesc.CLASS_NAME).equals(Ball.name)) {
            return Ball.createObj(desc, world);

        } else if(desc.getString(PhysicsObjectDesc.CLASS_NAME).equals(CodeDors.name)) {
            return CodeDors.createObj(desc, world);
        }

        return null;
    }

    private void updateId() {
        for(int i=0; i<actorList.size; i++) {
            ActorId tmp = actorList.get(i);
            if(tmp.getNeededActorId() != -1) {
                tmp.setNeededActorId(getActorWithId(tmp.getNeededActorId()));
            }
        }
    }

    private ActorId getActorWithId(int id) {
        for(int i=0; i<actorList.size; i++) {
            ActorId actor = actorList.get(i);
            if(actor.getId() == id) {
                return actor;
            }
        }
        throw new NullPointerException("Nie ma obiektu z ID " + id);
    }
}
