package me.admund.ggj2015.levels;

import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by admund on 2015-01-24.
 */
public class PhysicsObjectDesc {
    public static final String CLASS_NAME = "className";
    public static final String POS_X = "posX";
    public static final String POS_Y = "posY";

    public static final String SIZE_X = "sizeX";
    public static final String SIZE_Y = "sizeY";
    public static final String RADIUS = "radius";

    public static final String ID = "id";

    public static final String RANGE = "range";

    public static final String NEEDED_ACTOR_ID = "neededActorId";

    public static final String TEXTURE_NAME = "textureName";

    public ObjectMap<String, String> argMap = new ObjectMap<String, String>();

    public float getFloat(String argName) {
        return Float.valueOf(argMap.get(argName));
    }

    public int getInt(String argName) {
        return Integer.valueOf(argMap.get(argName));
    }

    public String getString(String argName) {
        return argMap.get(argName);
    }
}
