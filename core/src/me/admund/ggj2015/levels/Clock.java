package me.admund.ggj2015.levels;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import me.admund.framework.GameConfig;
import me.admund.framework.utils.FontUtils;

/**
 * Created by admund on 2015-01-25.
 */
public class Clock extends Actor {

    private float timePassed = 0;
    private boolean isStop = false;

    public void act(float delta) {
        if(!isStop) {
            timePassed += delta;
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        BitmapFont font = FontUtils.getBasicFont();
        font.setColor(Color.BLACK);
        font.setScale(1);

        String str = getTime();

        font.draw(batch, str, (GameConfig.GAME_WIDTH * 2 - font.getBounds(str).width) * .5f, GameConfig.GAME_HEIGHT * .95f);

    }

    public String getTime() {
        String time = "Time passed: ";
        float tmp = 0;

        int houreCnt = (int)(timePassed / 3600);
        time += ((houreCnt < 10) ? "0" : "") + houreCnt + ":";
        tmp = timePassed - houreCnt * 3600;

        int minuteCnt = (int)(tmp / 60);
        time += ((minuteCnt < 10) ? "0" : "") + minuteCnt + ":";
        tmp = tmp - minuteCnt * 60;

        time += ((tmp < 10) ? "0" : "") + format(tmp);//((int)(tmp * 1000)) / 1000f;//String.format("%.3f",  tmp);
        time = time.replace(",", ".");
        return time;
    }

    private String format(float tmp) {
        String str = "";
        int tmp2 = (int)tmp;
        str += tmp2;
        str += ".";
        int tmp3 = (int)(((tmp * 1000) - (tmp2 * 1000)));
        if(tmp3 < 10) {
            str += "00";
        } else if(tmp3 < 100){
            str += "0";
        }
        str += tmp3;
        return str;
    }

    public void stop() {
        isStop = true;
    }
}
