package me.admund.ggj2015.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import me.admund.framework.GameConfig;
import me.admund.framework.draw.SimpleTextureHolder;
import me.admund.framework.draw.parallaxa.Parallaxa;
import me.admund.framework.draw.parallaxa.ParallaxaLayer;
import me.admund.framework.draw.parallaxa.TextParallaxa;
import me.admund.framework.physics.Collisions;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.framework.scenes.AbstractScene;
import me.admund.ggj2015.GGJ2015Game;
import me.admund.ggj2015.GGJ2015ReuseFactory;
import me.admund.ggj2015.TextureList;
import me.admund.ggj2015.draw.WinDialog;
import me.admund.ggj2015.levels.Clock;
import me.admund.ggj2015.levels.Level;
import me.admund.ggj2015.player.Player;

/**
 * Created by admund on 2015-01-23.
 */
public class MainScene extends AbstractScene {
    private PhysicsWorld world = null;

    private Player player1 = null;
    private Player player2 = null;

    private Parallaxa paralaxa = null;

    private Clock clock = null;

    private String pin = createPin();

    private WinDialog dialog = null;

    @Override
    public void create() {
        world = new PhysicsWorld(new Vector2(0, -30), new GGJ2015ReuseFactory());
        world.setContactListner(new Collisions());

        createParallaxa();
        new Level().create(world, stage);
        createPlayer();

        clock = new Clock();

        dialog = new WinDialog();
        dialog.init();
        dialog.setClock(clock);
        //stage.addActor(dialog);

        System.out.println(pin);
    }

    @Override
    public void act(float deltaTime) {
        world.step();
        super.act(deltaTime);
        clock.act(deltaTime);

        checkWinCondition();
    }

    @Override
    public void draw(Batch batch) {
        // PLAYER 1
        Vector3 cameraTrans = player1.getCameraPos(getCurrentCamera().position);
        paralaxa.updatePos(cameraTrans);
        getCurrentCamera().translate(cameraTrans);
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight());
        super.draw(batch);
        //world.debugRender(stage.getCamera());

        // PLAYER 2
        cameraTrans = player2.getCameraPos(getCurrentCamera().position);
        paralaxa.updatePos(cameraTrans);
        getCurrentCamera().translate(cameraTrans);
        Gdx.gl.glViewport(Gdx.graphics.getWidth() / 2, 0, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight());
        super.draw(batch);
        //world.debugRender(stage.getCamera());

        batch.begin();
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        clock.draw(batch, 1);

        if(WIN) {
            dialog.draw(batch, 1);
        }
        batch.end();
    }

    @Override
    public void dispose() {
        super.dispose();
        world.dispose();
    }

    private void createParallaxa() {
        paralaxa = new Parallaxa();

        float X = -300;
        float Y = 0;

        float W = 600;
        float H = 76.8f * 2;//60;

        ParallaxaLayer layer0 = new ParallaxaLayer(0, new SimpleTextureHolder(TextureList.sky));
        layer0.init(X, Y, W, H);
        paralaxa.addLayer(layer0);

        ParallaxaLayer layer1 = new ParallaxaLayer(0, new SimpleTextureHolder(TextureList.sea1));
        layer1.init(X, Y, W, H);
        paralaxa.addLayer(layer1);

        ParallaxaLayer layer2 = new ParallaxaLayer(0f, new SimpleTextureHolder(TextureList.sea2));
        layer2.init(X, Y, W, H);
        paralaxa.addLayer(layer2);

        ParallaxaLayer layer3 = new ParallaxaLayer(0f, new SimpleTextureHolder(TextureList.sea3));
        layer3.init(X, Y, W, H);
        paralaxa.addLayer(layer3);

        TextParallaxa pin1Layer = new TextParallaxa(.5f, pin);
        pin1Layer.init(100, GGJ2015Game.WORLD_SIZE_Y * .5f , 7f, 4f);
        paralaxa.addLayer(pin1Layer);

        TextParallaxa pin2Layer = new TextParallaxa(.5f, pin);
        pin2Layer.init(-100, GGJ2015Game.WORLD_SIZE_Y * .5f, 7f, 4f);
        paralaxa.addLayer(pin2Layer);

        ParallaxaLayer layer4 = new ParallaxaLayer(0, new SimpleTextureHolder(TextureList.seaweed));
        layer4.init(X, Y, W, H);
        paralaxa.addLayer(layer4);

        ParallaxaLayer layer5 = new ParallaxaLayer(0, new SimpleTextureHolder(TextureList.submarine));
        layer5.init(X, Y, W, H);
        paralaxa.addLayer(layer5);

        stage.addActor(paralaxa);
    }

    private void createPlayer() {
        player1 = new Player(1);
        player1.create(world);
        player1.init(GGJ2015Game.WORLD_SIZE_X * -.5f + (Player.SIZE_X * 1), 20);
        //player1.init(150, 20);
        player1.setPin(pin);
        stage.addActor(player1);

        player2 = new Player(2);
        player2.create(world);
        player2.init(GGJ2015Game.WORLD_SIZE_X * .5f - (Player.SIZE_X * 2), 20);
        player2.setPin(pin);
        stage.addActor(player2);
    }

    private String createPin() {
        String pin = "";
        for (int i = 0; i < 4; i++) {
            pin += MathUtils.random(9);
        }
        return pin;
    }

    private float winY = 105;

    private boolean WIN = false;

    private void checkWinCondition() {
        if(player1.getPosition().y > winY) {
            player1.setWin();
        }

        if(player2.getPosition().y > winY) {
            player2.setWin();
        }

        if(player1.getPosition().y > winY && player2.getPosition().y > winY) {
            WIN = true;
            clock.stop();
        }
    }
}
