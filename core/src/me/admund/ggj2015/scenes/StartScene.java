package me.admund.ggj2015.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector3;
import me.admund.framework.GameConfig;
import me.admund.framework.draw.SimpleTextureHolder;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.framework.scenes.AbstractScene;
import me.admund.framework.scenes.ScenesManager;
import me.admund.ggj2015.draw.SimpleDrawObject;

/**
 * Created by admund on 2015-01-25.
 */
public class StartScene extends AbstractScene {

    @Override
    public void create() {
        SimpleDrawObject obj = new SimpleDrawObject(new SimpleTextureHolder("startpage.png"));
        obj.init(0, 0, PhysicsWorld.BOX_SCREEN_WIDTH, PhysicsWorld.BOX_SCREEN_HEIGHT);
        stage.addActor(obj);

        Gdx.input.setInputProcessor(new InputStart());
    }

    @Override
    public void draw(Batch batch) {
        // PLAYER 1
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        super.draw(batch);
    }

    class InputStart extends InputAdapter {

        @Override
        public boolean keyDown(int keycode) {
            if(keycode == Input.Keys.ENTER) {

                ScenesManager.inst().pop();
                ScenesManager.inst().push(new MainScene(), true);

                return true;
            }
            return false;
        }
    }
}
