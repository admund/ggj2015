package me.admund.ggj2015.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import me.admund.ggj2015.physics.connected.CodeDors;

/**
 * Created by admund on 2015-01-24.
 */
public class Code {
    public static int PIN_CNT = 4;

    private String pin = "1111";
    private String tryPIN = "";

    private CodeDors touchedDors = null;

    public boolean isOpen = false;

    private float time = 0f;
    private float MAX_TIME = 5f;

    private Sound accesDenid = null;
    private Sound ticking = null;

    public Code() {
        accesDenid = Gdx.audio.newSound(Gdx.files.internal("access.ogg"));
        ticking = Gdx.audio.newSound(Gdx.files.internal("code.ogg"));
    }

    public void setTouchedDors(CodeDors touchedDors) {
        if(!isTouched()) {
            this.touchedDors = touchedDors;
            time = MAX_TIME;

            ticking.play();
        }
    }

    public boolean isTouched() {
        return touchedDors != null;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public void addTryPIN(String character) {
        tryPIN += character;
        if(tryPIN.length() == PIN_CNT ) {
            if(tryPIN.equals(pin)) {
                touchedDors.open();
                isOpen = true;
            } else {
                reset();
            }
        }
    }

    public void act(float delta) {
        if(time != 0) {
            time -= delta;
            if(time < 0) {
                time = 0;
                ;//reset();
            }
        }
    }

    public boolean isOpen() {
        return isOpen;
    }

    private void reset() {
        //time = 0;
        tryPIN = "";
        //touchedDors = null;

        accesDenid.play();
    }
}
