package me.admund.ggj2015.player;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import me.admund.framework.scenes.ScenesManager;
import me.admund.ggj2015.scenes.MainScene;

/**
 * Created by admund on 2015-01-23.
 */
public class PlayerInput extends InputAdapter {

    private int player1 = 2;
    private int player2 = 1;

    private PlayerController playerController = null;

    public PlayerInput(PlayerController playerController) {
        this.playerController = playerController;
    }

    //public Array<Touch> touchList = new Array<Touch>();

    @Override
    public boolean keyDown(int keycode) {
        boolean result = false;
        switch (keycode) {
            case Input.Keys.RIGHT: {
                playerController.setMove(MoveEnum.RIGHT, true, player1);
                result = true;
            }
            break;

            case Input.Keys.LEFT: {
                playerController.setMove(MoveEnum.LEFT, true, player1);
                result = true;
            }
            break;

            case Input.Keys.UP: {
                playerController.jump(player1);
            }
            break;

            case Input.Keys.DOWN: {
                playerController.setInShelf(true, player1);
            }
            break;

            case Input.Keys.D: {
                playerController.setMove(MoveEnum.RIGHT, true, player2);
                result = true;
            }
            break;

            case Input.Keys.A: {
                playerController.setMove(MoveEnum.LEFT, true, player2);
                result = true;
            }
            break;

            case Input.Keys.W: {
                playerController.jump(player2);
            }
            break;

            case Input.Keys.S: {
                playerController.setInShelf(true, player2);
            }
            break;

            case Input.Keys.TAB: {
                swapPlayers();
            }
            break;

            case Input.Keys.ENTER: {
                resetMap();
            }
        }

        if(keycode >= Input.Keys.NUM_0 && keycode <= Input.Keys.NUM_9 && playerController.canWriteCode()) {
            playerController.addTryPIN(Input.Keys.toString(keycode));
        }
        return false;//result;
    }

    @Override
    public boolean keyUp(int keycode) {
        boolean result = false;
        switch (keycode) {
            case Input.Keys.RIGHT: {
                playerController.setMove(MoveEnum.RIGHT, false, player1);
                result = true;
            }
            break;

            case Input.Keys.LEFT: {
                playerController.setMove(MoveEnum.LEFT, false, player1);
                result = true;
            }
            break;

            case Input.Keys.A: {
                playerController.setMove(MoveEnum.LEFT, false, player2);
                result = true;
            }
            break;

            case Input.Keys.D: {
                playerController.setMove(MoveEnum.RIGHT, false, player2);
                result = true;
            }
            break;
        }
        return false;//result;
    }

    private void swapPlayers() {
        int tmp = player1;
        player1 = player2;
        player2 = tmp;

        resetMoves();
    }

    private void resetMoves() {
        playerController.resetMoves();
    }

    private void resetMap() {
        ScenesManager.inst().pop();
        ScenesManager.inst().push(new MainScene(), true);
    }
}
