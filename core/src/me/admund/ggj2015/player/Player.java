package me.admund.ggj2015.player;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import me.admund.framework.draw.AnimationState;
import me.admund.framework.draw.AnimationTextureHolder;
import me.admund.framework.draw.DrawUtils;
import me.admund.framework.physics.PhysicsObject;
import me.admund.framework.physics.PhysicsUtils;
import me.admund.framework.utils.TimeBoolean;
import me.admund.ggj2015.TextureList;
import me.admund.ggj2015.physics.CodeDorsElement;
import me.admund.ggj2015.physics.CollisionFilters;
import me.admund.ggj2015.physics.connected.PhysicsButton;
import me.admund.ggj2015.physics.Shelf;

/**
 * Created by admund on 2015-01-23.
 */
public class Player extends PhysicsObject {
    public static final float SIZE_X = 7f;
    public static final float SIZE_Y = 4f;

    private PlayerController controller = null;

    private boolean isDead = false;
    private TimeBoolean refreshBabyBoolean = new TimeBoolean(true);

    private int playerNr = 0;

    public Player(int playerNr) {
        super();
        controller = new PlayerController(this);
        this.playerNr = playerNr;
    }

    @Override
    public BodyDef getBodyDef() {
        BodyDef def = new BodyDef();
        def.fixedRotation = true;
        def.type = BodyDef.BodyType.DynamicBody;
        return def;
    }

    @Override
    public FixtureDef getFixtureDef() {
        FixtureDef def = new FixtureDef();
        def.shape = PhysicsUtils.getDefaultPolygonShape();
        def.density = 3f/(SIZE_X * SIZE_Y);
        def.friction = .5f;
        def.filter.categoryBits = CollisionFilters.CATEGORY_PLAYER;
        def.filter.maskBits = CollisionFilters.MASK_PLAYER;
        return def;
    }

    public void init(float posX, float posY) {
        super.init();
        setCurrentPos(posX, posY);
        setSize(SIZE_X, SIZE_Y);
        setOrigin(Align.center);

        //setTextureHolder(new SimpleTextureHolder(TextureList.player));
        AnimationTextureHolder anim = new AnimationTextureHolder();
        if(getPlayerNr() == 1) {
            anim.addAnimation(TextureList.hom_walk, AnimationState.MOVE, 2, 6);
            //anim.addAnimation(TextureList.hom_jump, AnimationState.JUMP, 2, 6);
        } else {
            anim.addAnimation(TextureList.krab_walk, AnimationState.MOVE, 2, 6);
            //anim.addAnimation(TextureList.hom_jump, AnimationState.JUMP, 2, 6);
        }
        setTextureHolder(anim);

        refreshBabyBoolean.change();
    }

    public void setPin(String pin) {
        controller.setPin(pin);
    }

    private boolean win = false;
    public void setWin() {
        win = true;
    }

    @Override
    public void beginContact(Contact contact, boolean isObjectA) {
        Fixture fixture = isObjectA ? contact.getFixtureB() : contact.getFixtureA();
        PhysicsObject obj = (PhysicsObject)PhysicsUtils.getObject(fixture.getBody());
        //System.out.println("beginContact " + obj);

        if(obj.isGround()) {
            boolean result = false;
            WorldManifold tmp = contact.getWorldManifold();
            for(int i=0; i<tmp.getNumberOfContactPoints(); i++) {

                float playerUpEdge = getPosition().y - SIZE_Y * .5f;

                if(playerUpEdge > tmp.getPoints()[i].y) {
                    result = true;
                }
            }
            if(result) {
                controller.addContactObject(obj);
            }
            //System.out.println("beginContact setGrounded - " + controller.isGrounded());
        }

        if(obj instanceof PhysicsButton) {
            ((PhysicsButton)obj).setPushed(true);
        }

        if(obj instanceof Shelf) {
            //System.out.println("beginContact " + obj);

            controller.setTouchShelf(true);

            WorldManifold tmp = contact.getWorldManifold();
            float playerUpEdge = getPosition().y + SIZE_Y * .5f;
            for(int i=0; i<tmp.getNumberOfContactPoints(); i++) {
                if(playerUpEdge < tmp.getPoints()[i].y) {
                    controller.setInShelf(true);
                }
            }
        }

        if(obj instanceof CodeDorsElement) {
            controller.setCanWriteCode(((CodeDorsElement)obj).getDors());
        }
    }

    @Override
    public void endContact(Contact contact, boolean isObjectA) {
        Fixture fixture = isObjectA ? contact.getFixtureB() : contact.getFixtureA();
        PhysicsObject obj = (PhysicsObject)PhysicsUtils.getObject(fixture.getBody());
        //System.out.println("endContact " + obj);
        if(obj.isGround()) {
            controller.removeGroundCnt(obj);
            //System.out.println("endContact setGrounded - " + controller.isGrounded());
        }

        if(obj instanceof PhysicsButton) {
            ((PhysicsButton)obj).setPushed(false);
        }

        if(obj instanceof Shelf) {
            //System.out.println("endContact " + obj);
            controller.setInShelf(false);
            controller.setTouchShelf(false);
        }
    }

    @Override
    public void preSolve(Contact contact, boolean isObjectA) {
        Fixture fixture = isObjectA ? contact.getFixtureB() : contact.getFixtureA();
        Object obj = PhysicsUtils.getObject(fixture.getBody());

        if(obj instanceof Shelf) {
            //System.out.println("preSolve " + obj);
            if(controller.isInShelf()) {
                contact.setEnabled(false);
            }
        }
    }

    @Override
    public void postSolve(Contact contact, boolean isObjectA) {}

    @Override
    public boolean isGround() {
        return false;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        //if(!win) {
            controller.update(delta);
        //}
    }

    public Vector3 getCameraPos(Vector3 cameraPos) {
        return controller.getCameraPos(cameraPos);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        DrawUtils.drawActor(batch, getTextureRegion(), this, controller.isFlipY(), false);
    }

    public boolean isDead() {
        return isDead;
    }

    public int getPlayerNr() {
        return playerNr;
    }
}
