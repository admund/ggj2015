package me.admund.ggj2015.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import me.admund.framework.GameConfig;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.ggj2015.physics.connected.CodeDors;

/**
 * Created by admund on 2015-01-23.
 */
public class PlayerController {
    private Player player = null;
    private Vector3 newCameraPos = new Vector3();

    private boolean isFlipY = false;

    private boolean goLeft = false;
    private boolean goRight = false;
    private boolean canJump = false;

    private Array<Object> contactObjectList = new Array<Object>();
    private boolean isInShelf = false;
    private boolean touchShelf = false;

    private Code code = new Code();

    public PlayerController(Player player) {
        this.player = player;
        ((InputMultiplexer) Gdx.input.getInputProcessor()).addProcessor(0, new PlayerInput(this));
    }

    public Vector3 getCameraPos(Vector3 cameraPos) {
        Vector3 transitionVector = new Vector3();

        Vector2 playerPos = player.getPosition();
        newCameraPos.x = playerPos.x * PhysicsWorld.BOX_TO_WORLD;
        transitionVector.x = -(cameraPos.x - newCameraPos.x);

        newCameraPos.y = playerPos.y * PhysicsWorld.BOX_TO_WORLD;
        if(newCameraPos.y < GameConfig.GAME_HEIGHT * .5f) {
            newCameraPos.y = GameConfig.GAME_HEIGHT * .5f;
        }

        if(newCameraPos.y > GameConfig.GAME_HEIGHT * 1.5f) {
            newCameraPos.y = GameConfig.GAME_HEIGHT * 1.5f;
        }

        transitionVector.y = -(cameraPos.y - newCameraPos.y);

        return transitionVector;
    }

    private float HORIZONTAL_IMPULS = 500f;
    private float VERTICAL_IMPULSE = 700f;

    private float HORIZONTAL_SPEED = 10f;
    private float VERTICAL_SPEED = 15f;

    private Vector2 impulseVec = new Vector2();
    private Vector2 speedVec = new Vector2();

    private float MAX_HORIZONTAL_SPEED = 15f;

    public void update(float delta) {
        impulseVec.set(0, 0);
        speedVec.set(0, player.getBody().getLinearVelocity().y);
        if(goLeft) {
            //speedVec.x = -HORIZONTAL_SPEED;
            impulseVec.x = -HORIZONTAL_IMPULS * delta;
            isFlipY = true;
        }
        if(goRight) {
            //speedVec.x = HORIZONTAL_SPEED;// * delta;
            impulseVec.x = HORIZONTAL_IMPULS * delta;
            isFlipY = false;
        }
        if(canJump) {
            canJump = false;
            impulseVec.y = VERTICAL_IMPULSE;// * delta;
        }

        player.getBody().applyForceToCenter(impulseVec, true);

        Vector2 linearVelocity = player.getBody().getLinearVelocity();
        if(Math.abs(linearVelocity.x) > MAX_HORIZONTAL_SPEED) {
            linearVelocity.x = MAX_HORIZONTAL_SPEED * Math.signum(linearVelocity.x);
            player.getBody().setLinearVelocity(linearVelocity);
        }
        code.act(delta);
    }

    public void setPin(String pin) {
        code.setPin(pin);
    }

    public void addContactObject(Object obj) {
        contactObjectList.add(obj);
        //System.out.println("increse " + contactObjectList.size);
    }

    public void removeGroundCnt(Object obj) {
        contactObjectList.removeValue(obj, true);
        //System.out.println("decres " + contactObjectList.size);
    }

    public boolean isGrounded() {
        return contactObjectList.size > 0;
    }

    public void setMove(MoveEnum move, boolean start, int playerNr) {
        if(player.getPlayerNr() == playerNr) {
            if (move == MoveEnum.LEFT) {
                goLeft = start;
            } else if (move == MoveEnum.RIGHT) {
                goRight = start;
            }
        }
    }

    public void jump(int playerNr) {
        if(player.getPlayerNr() == playerNr) {
            if (isGrounded() && !isInShelf) {
                canJump = true;
            }
        }
    }

    public boolean isFlipY() {
        return isFlipY;
    }

    public void resetMoves() {
        goLeft = false;
        goRight = false;
    }

    public void setTouchShelf(boolean touchShelf) {
        this.touchShelf = touchShelf;
    }

    public boolean isInShelf() {
        return isInShelf;
    }

    public void setInShelf(boolean isInShelf) {
        this.isInShelf = isInShelf;
    }

    public void setInShelf(boolean isInShelf, int playerNr) {
        if(player.getPlayerNr() == playerNr && touchShelf) {
            setInShelf(isInShelf);
        }
    }

    public void setCanWriteCode(CodeDors dors) {
        code.setTouchedDors(dors);
    }

    public boolean isOpen() {
        return code.isOpen();
    }

    public boolean canWriteCode() {
        return code.isTouched() && !isOpen();
    }

    public void addTryPIN(String str) {
        code.addTryPIN(str);
    }
}
