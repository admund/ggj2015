package me.admund.ggj2015.draw;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import me.admund.framework.GameConfig;
import me.admund.framework.draw.DrawObject;
import me.admund.framework.draw.DrawUtils;
import me.admund.framework.draw.SimpleTextureHolder;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.framework.utils.FontUtils;
import me.admund.ggj2015.GGJ2015Game;
import me.admund.ggj2015.levels.Clock;

/**
 * Created by admund on 2015-01-25.
 */
public class WinDialog extends DrawObject {

    private Clock clock = null;

    private float SIZE_X = GameConfig.GAME_WIDTH * PhysicsWorld.WORLD_TO_BOX;
    private float SIZE_Y = GameConfig.GAME_HEIGHT * PhysicsWorld.WORLD_TO_BOX * .5f;

    public void init(){//float posX, float posY, float sizeX, float sizeY) {
        //setPosition(posX, posY);
        //setSize(sizeX, sizeY);

        setPosition(SIZE_X * .5f, SIZE_Y * .25f);
        setSize(SIZE_X, SIZE_Y);

        setTextureHolder(new SimpleTextureHolder("highscore.png"));
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        DrawUtils.drawActor(batch, getTexture(), this);

        BitmapFont font = FontUtils.getBasicFont();
        font.setColor(Color.WHITE);
        font.draw(batch, "Your time is: " + clock.getTime(), GameConfig.GAME_WIDTH * .5f, GameConfig.GAME_HEIGHT * .3f);
    }

    public void setClock(Clock clock) {
        this.clock = clock;
    }
}
