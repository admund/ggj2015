package me.admund.ggj2015.draw;

import com.badlogic.gdx.graphics.g2d.Batch;
import me.admund.framework.draw.DrawObject;
import me.admund.framework.draw.DrawUtils;
import me.admund.framework.draw.ITextureHolder;
import me.admund.framework.draw.SimpleTextureHolder;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.ggj2015.GGJ2015Game;
import me.admund.ggj2015.TextureList;
import me.admund.ggj2015.levels.PhysicsObjectDesc;

/**
 * Created by admund on 2015-01-23.
 */
public class SimpleDrawObject extends DrawObject {
    public static final String name = "SimpleDrawObject";

    public static SimpleDrawObject createObj(PhysicsObjectDesc desc, PhysicsWorld world) {
        SimpleDrawObject drawObj = new SimpleDrawObject(
                new SimpleTextureHolder(desc.getString(PhysicsObjectDesc.TEXTURE_NAME)));
        drawObj.init(desc.getFloat(PhysicsObjectDesc.POS_X), desc.getFloat(PhysicsObjectDesc.POS_Y),
                desc.getFloat(PhysicsObjectDesc.SIZE_X), desc.getFloat(PhysicsObjectDesc.SIZE_Y));
        return drawObj;
    }

    public SimpleDrawObject(ITextureHolder holder) {
        setTextureHolder(holder);
    }

    public void init(float posX, float posY, float sizeX, float sizeY) {
        setPosition(posX, posY);
        setSize(sizeX, sizeY);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        DrawUtils.drawActor(batch, getTexture(), this);
    }
}
