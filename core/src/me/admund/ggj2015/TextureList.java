package me.admund.ggj2015;

/**
 * Created by admund on 2015-01-23.
 */
public class TextureList {
    public static final String player = "krab.png";

    public static final String kadlub = "kadlub1.png";

    public static final String shelf = "shelf2.png";

    public static final String seaweed = "seaweed1.png";
    public static final String sea1 = "sea1.png";
    public static final String sea2 = "sea2.png";
    public static final String sea3 = "sea3.png";
    public static final String sky = "sky_and_island.png";

    public static final String submarine = "submarine2.png";//"submarine_backdrop.png";

    public static final String bucket = "bucket1.png";

    public static final String hom_walk = "hom_walk.png";
    public static final String hom_jump = "hom_jump.png";

    public static final String krab_walk = "krab_walk.png";

    public static final String tv = "tv.png";

    public static final String wall = "wall5.png";
}
