package me.admund.framework.physics;

import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by admund on 2015-01-24.
 */
public class ActorId extends Actor {
    private int id = -1;

    private int neededActorId = -1;

    public int getId() {
        return id;
    }

    public int getNeededActorId() {
        return neededActorId;
    }

    public void setNeededActorId(int actorId) {
        neededActorId = actorId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNeededActorId(ActorId actor) {
        // TODO
    }
}
