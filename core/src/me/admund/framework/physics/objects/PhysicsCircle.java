package me.admund.framework.physics.objects;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import me.admund.framework.physics.PhysicsObject;
import me.admund.framework.physics.PhysicsUtils;

/**
 * Created by admund on 2015-01-24.
 */
public class PhysicsCircle extends PhysicsObject {

    @Override
    public BodyDef getBodyDef() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.bullet = true;
        return bodyDef;
    }

    @Override
    public FixtureDef getFixtureDef() {
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = PhysicsUtils.getDefaultCircleShape();
        return fixtureDef;
    }

    @Override
    public boolean isGround() {
        return false;
    }

    @Override
    public void beginContact(Contact contact, boolean isObjectA) {}

    @Override
    public void endContact(Contact contact, boolean isObjectA) {}

    @Override
    public void preSolve(Contact contact, boolean isObjectA) {}

    @Override
    public void postSolve(Contact contact, boolean isObjectA) {}
}
