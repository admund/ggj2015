package me.admund.framework.draw;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectMap;

import java.util.Iterator;

/**
 * Created by admund on 2015-01-24.
 */
public class AnimationRepo implements Disposable {
    private static final AnimationRepo inst = new AnimationRepo();
    private AnimationRepo(){}
    public static AnimationRepo inst(){
        return inst;
    }

    private ObjectMap<String, Animation> animationMap = new ObjectMap<String, Animation>();

    public Animation getAnimation(String name, int rowFrameCnt, int colFrameCnt) {
        if(name == null) {
            return null;
        }
        Animation result = animationMap.get(name);
        if(result == null) {
            result = createAnimation(name, rowFrameCnt, colFrameCnt);
            animationMap.put(name, result);
        }
        return result;
    }

    @Override
    public void dispose() {
//        Iterator<ObjectMap.Entry<String, Animation>> iterator = animationMap.iterator();
//        while(iterator.hasNext()) {
//            ObjectMap.Entry<String, Animation> next = iterator.next();
//            if(next.value != null) {
//                next.value.dispose();
//            }
//        }
    }

    private Animation createAnimation(String spriteSheetName, int rowFrameCnt, int colFrameCnt) {
        Texture sheet = TextureRepo.inst().getTexture(spriteSheetName);
        TextureRegion[][] tmp = TextureRegion.split(sheet, sheet.getWidth()/colFrameCnt, sheet.getHeight()/rowFrameCnt);              // #10
        TextureRegion[] textureRegionsTab = new TextureRegion[colFrameCnt * rowFrameCnt];
        int index = 0;
        for (int i = 0; i < rowFrameCnt; i++) {
            for (int j = 0; j < colFrameCnt; j++) {
                textureRegionsTab[index++] = tmp[i][j];
            }
        }
        return new Animation(0.05f, textureRegionsTab);
    }
}
