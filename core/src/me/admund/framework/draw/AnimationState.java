package me.admund.framework.draw;

/**
 * Created by admund on 2015-01-24.
 */
public enum AnimationState {
    MOVE, JUMP, IDLE
}
