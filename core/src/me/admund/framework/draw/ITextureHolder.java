package me.admund.framework.draw;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by admund on 2014-12-29.
 */
public interface ITextureHolder {
    public Texture getTexture();
    public TextureRegion getTextureRegion();
    public void changeAnimationState(AnimationState state);
    public void act(float delta);
}
