package me.admund.framework.draw;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by admund on 2015-01-24.
 */
public class AnimationTextureHolder implements ITextureHolder {
    private ObjectMap<AnimationState, Animation> aniamationMap = new ObjectMap<AnimationState, Animation>();
    private float stateTime;
    private Animation currentAnimation = null;

    public void addAnimation(String sheetName, AnimationState state, int frameRowCnt, int frameColCnt) {
        Animation anim = AnimationRepo.inst().getAnimation(sheetName, frameRowCnt, frameColCnt);
        aniamationMap.put(state, AnimationRepo.inst().getAnimation(sheetName, frameRowCnt, frameColCnt));
        if(currentAnimation == null) {
            currentAnimation = anim;
        }
    }

    @Override
    public Texture getTexture() {
        return null;
    }

    @Override
    public TextureRegion getTextureRegion() {
        return currentAnimation.getKeyFrame(stateTime, true);
    }

    @Override
    public void act(float delta) {
        stateTime += delta;
    }

    @Override
    public void changeAnimationState(AnimationState state) {
        Animation anim = aniamationMap.get(state);
        if(anim != null) {
            currentAnimation = anim;
            stateTime = 0;
        }
    }
}
