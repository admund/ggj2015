package me.admund.framework.draw.parallaxa;

import com.badlogic.gdx.graphics.g2d.Batch;
import me.admund.framework.draw.DrawObject;
import me.admund.framework.draw.DrawUtils;
import me.admund.framework.draw.ITextureHolder;

/**
 * Created by admund on 2015-01-22.
 */
public class ParallaxaLayer extends AbstractParallaxaLayer {
    private float value = 0f;

    public ParallaxaLayer(float value, ITextureHolder holder) {
        super(value);
        setTextureHolder(holder);
        this.value = value;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        DrawUtils.drawActor(batch, getTexture(), this);
    }
}
