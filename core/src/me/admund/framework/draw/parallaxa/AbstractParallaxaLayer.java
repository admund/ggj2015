package me.admund.framework.draw.parallaxa;

import me.admund.framework.draw.DrawObject;

/**
 * Created by admund on 2015-01-24.
 */
public abstract class AbstractParallaxaLayer extends DrawObject {
    protected float value = 0f;

    public AbstractParallaxaLayer(float value) {
        this.value = value;
    }

    public void init(float posX, float posY, float sizeX , float sizeY) {
        setPosition(posX, posY);
        setSize(sizeX, sizeY);
    }

    public void updatePos(float cameraTransitionX , float cameraTransitionY) {
        setX(getX() + value * cameraTransitionX);
        //setY(/*getY() + */value * cameraTransitionY);
    }
}
