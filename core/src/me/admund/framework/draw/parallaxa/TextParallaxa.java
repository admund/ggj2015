package me.admund.framework.draw.parallaxa;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import me.admund.framework.draw.DrawUtils;
import me.admund.framework.draw.SimpleTextureHolder;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.framework.utils.FontUtils;
import me.admund.ggj2015.TextureList;

/**
 * Created by admund on 2015-01-24.
 */
public class TextParallaxa extends AbstractParallaxaLayer {

    private String PIN  = null;

    private float sizeX = 0;
    private float sizeY = 0;

    public TextParallaxa(float value, String PIN) {
        super(value);
        this.PIN = PIN;
    }

    @Override
    public void init(float posX, float posY, float sizeX, float sizeY) {
        super.init(posX, posY, sizeX, sizeY);
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        setOrigin(Align.center);
        setTextureHolder(new SimpleTextureHolder(TextureList.tv));
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        DrawUtils.drawActor(batch, getTexture(), this);

        BitmapFont font = FontUtils.getBasicFont();
        font.setScale(.5f);
        font.setColor(Color.argb8888(0, 1, 1, .2f));
        font.draw(batch, PIN, (getX() - sizeX * .3f) * PhysicsWorld.BOX_TO_WORLD, (getY() + sizeY * .1f) * PhysicsWorld.BOX_TO_WORLD);
    }

    @Override
    public void updatePos(float cameraTransitionX, float cameraTransitionY) {
        //System.out.println();
        setX(getX() + value * cameraTransitionX);
    }
}
