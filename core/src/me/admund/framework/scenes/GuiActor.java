package me.admund.framework.scenes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import me.admund.framework.GameConfig;
import me.admund.framework.utils.FontUtils;

/**
 * Created by admund on 2015-01-24.
 */
public class GuiActor extends Actor {

    @Override
    public void draw(Batch batch, float parentAlpha) {
        BitmapFont font = FontUtils.getBasicFont();
        font.setColor(Color.rgba8888(100, 0, 105, 50));
        font.draw(batch, "543534", 120, GameConfig.GAME_HEIGHT * .5f + 60);

        font.setColor(Color.argb8888(50, 0, 105, 100));
        font.draw(batch, "543534", -120, GameConfig.GAME_HEIGHT * .5f + 55);
    }
}
